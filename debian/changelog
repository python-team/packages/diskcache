diskcache (5.6.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Ran wrap-and-sort -bastk.
  * Drop enable_newer_django.patch.
  * Rebased test-command-m.patch.
  * Cleans better.

 -- Thomas Goirand <zigo@debian.org>  Thu, 18 Apr 2024 10:37:11 +0200

diskcache (5.4.0-2) experimental; urgency=medium

  [ Stefano Rivera ]
  * Team upload.
  * Adjust enable_newer_django.patch for tox 4 compatibility.
    (Closes: #1035636)
  * Use python -m pytest, instead of pytest-3, to run tests.
  * Build-Depend on python3-wheel, to allow pybuild to build a wheel for tox.

  [ Debian Janitor ]
  * Update standards version to 4.6.1, no changes needed.

 -- Stefano Rivera <stefanor@debian.org>  Thu, 11 May 2023 14:45:22 -0400

diskcache (5.4.0-1) unstable; urgency=medium

  * New upstream version
    Closes: #997505
  * Fix watchfile to detect new versions on github
  * Standards-Version: 4.6.0 (routine-update)
  * Enable django>=2.2
  * Build-Depends: python3-typing-extensions
  * Add missing build dependency on dh addon.
  * Call correct executable pytest-3
  * Do not provide docs in /usr/lib/python3/dist-packages
    Closes: #1002823

 -- Andreas Tille <tille@debian.org>  Fri, 21 Jan 2022 21:49:30 +0100

diskcache (5.2.1-2) unstable; urgency=medium

  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 04 Feb 2021 22:26:33 +0100

diskcache (5.2.1-1) unstable; urgency=low

  * Initial release (Closes: #940625)

 -- Andreas Tille <tille@debian.org>  Tue, 26 Jan 2021 09:33:31 +0100
